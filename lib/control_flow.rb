# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.reject { |chr| /[[:lower:]]/.match(chr) }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  half_str = str.length / 2
  str.length.odd? ? str[half_str] : str[half_str - 1] + str[half_str]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.inject(0) { |acc, c| VOWELS.include?(c) ? acc + 1 : acc + 0 }
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  arr.each_with_index do |el, idx|
    string << el
    string << separator unless idx == arr.length - 1
  end
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.downcase.chars.map.with_index(1) { |c, idx| idx.even? ? c.upcase : c }.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split(" ").map { |word| word.length > 4 ? word.reverse : word }.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |num|
    case
    when num % 3 == 0 && num % 5 == 0
      "fizzbuzz"
    when num % 3 == 0
      "fizz"
    when num % 5 == 0
      "buzz"
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  idx = -1
  arr.length.times do
    reversed << arr[idx]
    idx -= 1
  end
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2...num).each { |i| (return false) if num % i == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |i| num % i == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |i| i if prime?(i) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  arr.each { |el| el.odd? ? odds << el : evens << el }
  odds.count == 1 ? odds[0] : evens[0]
end
